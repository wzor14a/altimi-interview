import { IData } from './getDataFromFile';

const formatData = (date: Date) => {
  // similar to getISOString but without 'ms' and 'Z'
  const pad = (number: number) => (number < 10 ? '0' + number : number);

  return (
    date.getUTCFullYear() +
    '-' +
    pad(date.getUTCMonth() + 1) +
    '-' +
    pad(date.getUTCDate()) +
    'T' +
    pad(date.getUTCHours()) +
    ':' +
    pad(date.getUTCMinutes()) +
    ':' +
    pad(date.getUTCSeconds())
  );
};

const fillInMissingData = (data: IData[], emptyCPMU = 'No Value'): IData[] => {
  let filledData: IData[] = [];
  let sortedDataByDate = data
    .map((dataObject) => ({ ...dataObject, _Month: new Date(dataObject.Month) }))
    .sort((a, b) => a._Month.getDate() - b._Month.getDate());

  for (let i = 0; i < sortedDataByDate.length - 1; i++) {
    const yearDiff =
      sortedDataByDate[i + 1]._Month.getFullYear() - sortedDataByDate[i]._Month.getFullYear();
    const monthDiff =
      sortedDataByDate[i + 1]._Month.getMonth() - sortedDataByDate[i]._Month.getMonth();
    const fullMonthDiff = yearDiff * 12 + monthDiff;

    const { _Month, ...finalDataObject } = sortedDataByDate[i];
    filledData.push(finalDataObject);

    for (let monthIndex = 1; monthIndex < fullMonthDiff; monthIndex++) {
      filledData.push({
        ...finalDataObject,
        Month: formatData(new Date(new Date(_Month).setMonth(_Month.getMonth() + monthIndex))),
        CPMU: emptyCPMU,
      });
    }
  }

  if (sortedDataByDate.length > 0) {
    const { _Month, ...finalDataObject } = sortedDataByDate[sortedDataByDate.length - 1];
    filledData.push(finalDataObject);
  }

  return filledData;
};

export default fillInMissingData;
