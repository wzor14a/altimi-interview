import fs from 'fs';

type TFile = 'json';
export interface IData {
  Quarter?: number;
  Month: string;
  Complaints?: number;
  UnitsSold?: number;
  CPMU?: number | string | null;
}

const getJsonData = async () => {
  try {
    const data: string = await fs.promises.readFile(`${__dirname}/../../staticFiles/data.json`, {
      encoding: 'utf-8',
    });
    const dataArray: IData[] = JSON.parse(data);

    return dataArray;
  } catch (error) {
    throw error;
  }
};

const getDataFromFile = (file: TFile = 'json') => {
  if (file === 'json') {
    return getJsonData();
  } // else if (file === 'csv') { // I didn' know should I add getting data from csv file.
  //   return getCsvData();
  // }
};

export default getDataFromFile;
