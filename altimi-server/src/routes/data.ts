import { Router, Request, Response } from 'express';

import getDataFromFile, { IData } from '../calculateData/getDataFromFile';
import fillInMissingData from '../calculateData/fillInMissingData';

const dataRoute = Router();

dataRoute.get('/cpmu', async (req: Request, res: Response) => {
  const UNITS_COUNT = 1000000;
  const NO_CPMU_VALUE = 'No Value';

  try {
    let dataArray: IData[] = (await getDataFromFile()) as IData[];

    dataArray = dataArray.map((dataObject: IData) => {
      let CPMU: number | string | null =
        (+(dataObject.Complaints ?? 0) * UNITS_COUNT) / +(dataObject.UnitsSold ?? 0);

      if (!Number.isFinite(CPMU)) {
        CPMU = NO_CPMU_VALUE;
      }

      return { Month: dataObject.Month, CPMU };
    });

    dataArray = fillInMissingData(dataArray, NO_CPMU_VALUE);

    res.json(dataArray);
  } catch (error) {
    res.statusCode = 503;
    res.json({ error: 'CPMU data are not available. Please try again later' });
  }
});

export default dataRoute;
