import { Application, Router } from 'express';

import dataRoute from './data';

const routes: { [path: string]: Router } = {
  // Add all routes here
  '/data': dataRoute,
};

export const connectRoutes = (app: Application) => {
  for (let route in routes) {
    app.use(route, routes[route]);
  }
};
