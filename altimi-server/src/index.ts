import express, { Request, Response } from 'express';
import { connectRoutes } from './routes';

const app = express();

connectRoutes(app);

app.all('/*', (req: Request, res: Response) => {
  res.json({ message: 'Application is working' });
});

app.listen(4000, () => {
  console.log('Server is running on localhost:4000');
});
