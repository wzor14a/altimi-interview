
## To run application

### Server

`> cd altimi-server`

`> npm run dev` - run in developement mode

#### Debug

`> npm run debug` - run in debug mode (inspect mode)

#### Craete production code

`> npm run build` - build production application

`> npm start` - run production application - run it after build command

### UI

`> cd altimi-ui`

`> npm start` - run in development mode

#### Create production mode

`> npm run build` - build production application
