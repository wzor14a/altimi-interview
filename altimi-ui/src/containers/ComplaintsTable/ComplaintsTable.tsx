import React, { useState, useEffect } from 'react';

import ComplaintsTitle from '../../components/ComplaintsTitle';
import ComplaintsTableContent from '../../components/ComplaintsTableContent';

interface IProps {
  component?: React.ElementType;
}

const ComplaintsTable: React.FC<IProps> = ({ component: Component = 'div' }) => {
  const [data, setData] = useState<any[]>([]);
  const title = 'Complaints per milion units';

  // Fetch data in componentDidMount
  useEffect(() => {
    (async function () {
      try {
        let data: any = await fetch('/data/cpmu');
        data = await data.json();

        data = data.map((dataItem: any) => ({
          ...dataItem,
          Month: new Date(dataItem.Month),
        }));

        setData(data);
      } catch {}
    })();
  }, []);

  return (
    <Component>
      <ComplaintsTitle title={title} />
      <ComplaintsTableContent data={data} />
    </Component>
  );
};

export default ComplaintsTable;
