import React from 'react';
import classes from '../../utils/classes';

import styles from './MainContent.module.scss';

interface IProps {
  component?: React.ElementType;
  className?: string;
  [prop: string]: any;
}

const MainContent: React.FC<IProps> = ({
  component: Component = 'div',
  className = '',
  children,
  ...props
}) => (
  <Component className={classes(styles.MainContent, className)} {...props}>
    {children}
  </Component>
);

export default MainContent;
