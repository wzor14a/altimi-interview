type TClassesArgs = string | { [className: string]: boolean };

const classes = (...args: TClassesArgs[]): string => {
  const classArray: string[] = [];

  for (let arg of args) {
    if ('string' === typeof arg) {
      if (arg) {
        classArray.push(arg);
      }
    } else {
      for (let className in arg) {
        if (className && arg[className]) {
          classArray.push(className);
        }
      }
    }
  }

  return classArray.join(' ');
};

export default classes;
