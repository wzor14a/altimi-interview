import classes from './classes';

describe('classes', () => {
  test('empty', () => {
    const result = classes();

    expect(result).toBe('');
  });

  test('only strings', () => {
    const classNames = ['test1', 'className', '', 'other'];
    const result = classes(...classNames);

    expect(result).toBe(classNames.join(' ').replace(/\s+/g, ' '));
  });

  test('only objects', () => {
    const obj1 = {
      test1: true,
      test2: false,
      test3: !'test',
      test4: !!'test',
      test5: !0,
    };
    const obj2 = {
      test11: true,
      test12: false,
      test13: !'test',
      test14: !!'test',
      test15: !1,
    };

    expect(classes(obj1, obj2)).toBe('test1 test4 test5 test11 test14');
  });

  test('string and object', () => {
    const obj1 = {
      test1: true,
      test2: false,
      test3: !'test',
      test4: !!'test',
      test5: !0,
    };
    const obj2 = {
      test11: true,
      test12: false,
      test13: !'test',
      test14: !!'test',
      test15: !1,
    };

    expect(classes('className', obj1, obj2, 'other')).toBe(
      'className test1 test4 test5 test11 test14 other'
    );
  });
});
