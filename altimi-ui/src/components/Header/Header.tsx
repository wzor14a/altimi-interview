import React from 'react';
import classes from '../../utils/classes';

import styles from './Header.module.scss';

interface IProps {
  component?: React.ElementType;
  className?: string;
  children?: React.ReactChild;
  [prop: string]: any;
}

const Header: React.FC<IProps> = ({
  component: Component = 'div',
  className = '',
  children,
  ...props
}) => <Component className={classes(styles.Header, className)}>{children}</Component>;

export default Header;
