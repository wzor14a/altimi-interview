import React from 'react';
import classes from '../../utils/classes';

import styles from './ComplaintsTitle.module.scss';

interface IProps {
  component?: React.ElementType;
  className?: string;
  title?: string;
  [prop: string]: any;
}

const ComplaintsTitle: React.FC<IProps> = ({
  component: Component = 'h2',
  className = '',
  title,
  ...props
}) => (
  <Component className={classes(styles.ComplaintsTitle, className)} {...props}>
    {title}
  </Component>
);

export default ComplaintsTitle;
