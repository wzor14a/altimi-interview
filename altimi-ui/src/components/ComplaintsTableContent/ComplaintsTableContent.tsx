import React from 'react';
import classes from '../../utils/classes';

import styles from './ComplaintsTableContent.module.scss';

const months = [
  'January',
  'February',
  'March',
  'May',
  'June',
  'July',
  'September',
  'October',
  'November',
  'December',
];
const pad = (number: number) => (number < 10 ? '0' + number : number);

interface IProps {
  component?: React.ElementType;
  className?: string;
  data: { Month: Date; CPMU: number | string }[];
  [props: string]: any;
}

const ComplaintsTableContent: React.FC<IProps> = ({
  component: Component = 'table',
  className = '',
  data,
  ...props
}) => {
  if (!data.length) {
    return null;
  }

  let header = [];

  for (let dataItemTitle in data[0]) {
    header.push(dataItemTitle);
  }

  return (
    <Component className={classes(styles.ComplaintsTableContent, className)} {...props}>
      <thead>
        <tr>
          {header.map((h) => (
            <th key={h}>{h}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {data.map((dataItem, index) => (
          <tr key={index}>
            {(function () {
              let tableItems: React.ReactElement[] = [];

              for (let tableItem in dataItem) {
                let tableItemContent = (dataItem as any)[tableItem];

                if (tableItemContent instanceof Date) {
                  tableItemContent = `${pad(tableItemContent.getUTCDate())} ${
                    months[tableItemContent.getMonth()]
                  } ${tableItemContent.getUTCFullYear()}`;
                }

                tableItems.push(<td key={tableItem}>{tableItemContent}</td>);
              }

              return tableItems;
            })()}
          </tr>
        ))}
      </tbody>
    </Component>
  );
};

export default ComplaintsTableContent;
