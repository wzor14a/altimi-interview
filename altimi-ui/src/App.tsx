import React from 'react';
import './App.css';

import Header from './components/Header';
import ComplaintsTable from './containers/ComplaintsTable/ComplaintsTable';
import MainContent from './containers/MainContent/MainContent';

function App() {
  return (
    <div className="App">
      <Header />
      <MainContent>
        <ComplaintsTable />
      </MainContent>
    </div>
  );
}

export default App;
